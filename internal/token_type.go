package internal

type TokenType int

const (
	UNDEFINED TokenType = iota //Undefined type

	MOV // Instructions
	CALL
	ADD
	JE
	CMP
	PUSH
	POP
	JMP
	DIV
	RET

	INT // Arg types
	REGISTER

	LABEL // Scope defining
)

func (t TokenType) String() string {
	return [...]string{"UNDEFINED",
		"MOV",
		"CALL",
		"ADD",
		"JE",
		"CMP",
		"PUSH",
		"POP",
		"JMP",
		"DIV",
		"RET",
		"INT",
		"REGISTER",
		"LABEL",
	}[t]
}
