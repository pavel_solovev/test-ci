package internal

type Lexer struct {
	Code   string
	Tokens []*Token
}

func NewLexer(Code string) *Lexer {
	return &Lexer{
		Code:   Code,
		Tokens: make([]*Token, 0),
	}
}

func (l *Lexer) Split() {
	var tokenTemp string

	for _, rn := range l.Code {
		if l.isSplitSymbol(rn) {
			if len(tokenTemp) > 0 {
				tkn := NewToken(tokenTemp)
				l.Tokens = append(l.Tokens, tkn)
				tokenTemp = ""
			}
		} else {
			tokenTemp += string(rn)
		}
	}

	if len(tokenTemp) > 0 {
		tkn := NewToken(tokenTemp)
		l.Tokens = append(l.Tokens, tkn)
		tokenTemp = ""
	}
}

func (l *Lexer) Print() {
	for _, tk := range l.Tokens {
		tk.PrintToken()
	}
}

func (l *Lexer) isSplitSymbol(symbol rune) bool {
	return symbol == '\n' || symbol == ' ' || symbol == ',' || symbol == ':'
}
