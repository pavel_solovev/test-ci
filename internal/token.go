package internal

import (
	"fmt"
	"strconv"
)

var REGISTERS = [...]string{"rax", "rbx", "rdi", "rsi", "r8", "r9"}

type Token struct {
	Lexeme     string
	TokenValue TokenType
}

func NewToken(Lexeme string) *Token {
	return &Token{
		Lexeme:     Lexeme,
		TokenValue: detectTokenType(Lexeme),
	}
}

func detectTokenType(lexeme string) TokenType {
	if isInt(lexeme) {
		return INT
	}
	if isRegister(lexeme) {
		return REGISTER
	}
	if isLabel(lexeme) {
		return LABEL
	}

	switch lexeme {
	case "mov":
		return MOV
	case "call":
		return CALL
	case "add":
		return ADD
	case "je":
		return JE
	case "cmp":
		return CMP
	case "push":
		return PUSH
	case "pop":
		return POP
	case "jmp":
		return JMP
	case "div":
		return DIV
	case "ret":
		return RET
	default:
		return UNDEFINED
	}
}

func (t *Token) PrintToken() {
	fmt.Printf("Token: Type { %s } Value { %s }\n", t.TokenValue.String(), t.Lexeme)
}

func isInt(lex string) bool {
	_, err := strconv.Atoi(lex)
	return err == nil
}

func isRegister(lex string) bool {
	for _, reg := range REGISTERS {
		if reg == lex {
			return true
		}
	}
	return false
}

func isLabel(lex string) bool {
	for _, ch := range lex {
		if ch == '_' {
			return true
		}
		return false
	}
	return false
}
