package main

import "comp/internal"

func main() {
	lx := internal.NewLexer("_start:\n     mov rdi, 1000\n     mov rsi, 5\n     call _euler\n     mov r9, rax\n     mov rsi, 3\n     call _euler\n     add r9, rax\n     call _exit")
	lx.Split()
	lx.Print()
}
